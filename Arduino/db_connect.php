<?php
	class DB_CONNECT{
		function __construct(){
			$this->connect();
		}
		function __destruct(){
			$this->close();
		}
		function connect(){
			require_once __DIR__ . '/db_config.php';
			$con = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die ("Error, ".mysql_error());
			$bd = mysql_select_db(DB_DATABASE) or die("Error, ".mysql_error());
			return $con;
		}
		function close(){
			mysql_close();
		}

	}
?>