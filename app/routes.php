<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
Route::get('/saludo', function()
{
	return "Hola, petición procesada te saluda.";
	//return View::make('hello');
});*/
/*
Route::get('saludo/{nombre?}/{apellido?}', function ($nombre=null, $apellido=null){
	if ($nombre) {
		return "hola $nombre $apellido";
	}else{
		return "desconocido";
	}
});*/
/*
Route::get('test1', function(){
   //Registro 1
   $post1 = new Post();
   $post1->title = 'Título 1';
   $post1->body = 'Contenido 1';
   $post1->user_id = 1;
   $post1->save();
   //Registro 1
   $post2 = new Post();
   $post2->title = 'Título 2';
   $post2->body = 'Contenido 2';
   $post2->user_id = 1;
   $post2->save();
   //Registro 1
   $post3 = new Post();
   $post3->title = 'Título 3';
   $post3->body = 'Contenido 3';
   $post3->user_id = 1;
   $post3->save();
});*/
/*
Route::get('test1', function(){
   $user = User::find(3);
   $posts = $user->posts;
   $lista = '<ul>';
   foreach($posts as $item){
      $lista .= '<li>';
      $lista .= '<h2> ' . $item['title'] . ' </h2>';
      $lista .= '<div> ' . $item['body'] . ' </div>';
      $lista .= '</li>';
   }
   $lista .= '</ul>';
   return $lista;
});*/


/*
Route::get('test', function (){
	$user = new User();
	$user->email = 'fa@gmail.com';
	$user->real_name ='juan';
	$user->password =Hash::make('1234');
	$user->level = 1;
	$user->active = true;
	$user->save();
	return 'Usuario agregado';
});

Route::get('testget', function (){
	$user = User::find(1);
	return $user->real_name;
});

Route::get('testedit', function () {
	$user = User::where('real_name', 'juan')->first();
	return $user->email;
});
*/
Route::get('/', function (){
   return Redirect::to('auth/panel');
   #return View::make('hello');
});
/*
Route::get('testaa', function(){
   $user = User::find(2);
   $posts = $user->posts;
   //$lista = '<ul>';
   /*
   foreach($posts as $item){
      $lista .= '<li>';
      $lista .= '<h2> ' . $item['title'] . ' </h2>';
      $lista .= '<div> ' . $item['body'] . ' </div>';
      $lista .= '</li>';
   }
   $lista .= '</ul>';
   return $posts;
});
*/

//Insert Production Arduino ----------------------------------
/*
Route::get('insert', function () {
   $pro = new Production();
   $pro->user_id = 2;
   $pro->line_id = 1;
   $pro->model_id = 2;
   $pro->arduino_id = 1;
   if($pro->save()){
      echo "1";
   }else{
      echo "0";
   }
   
});*/
/*
Route::get('verdatos', function () {

   return $production = Production::with('model', 'line', 'user')->select(array('model_id'))->groupBy('line_id')->get();

   //$books = Book::with('author', 'publisher')->get();
   /*return $models = Line::find(1)->modelos;

   return $lines = Model::find(1)->lines;*

   //return $production = $model->production;
   //return $model;
   /*$data = '';
   foreach ($production as $item) {
      $data .= $item['lin_description'];
   }
   return $data;
   //return View::make('productions.index')->with('production', $model);
});*/
/*
Route::get('totalProduction', function () {
   $line = Line::find(1);
   $production = $line->productions;

   foreach ($production as $item) {
      echo $item['user_id'];
      
   }

});*/
/*
Route::get('testdel', function () {
	$user = User::find(1);
	$user->delete();
	return 'registro eliminado';
});

Route::get('testall', function () {
	$users = User::all();
	$nombre = '';
	foreach ($users as $item) {
		$nombre .= $item->real_name . '<br />';
	}
	return $nombre;
});

Route::get('testact', function () {
	$users = User::where('active', true)->orderBy('real_name')->get();
   	$nombres = '';
   	foreach($users as $item){
   	   $nombres .= $item->real_name . '<br />';
   	}
   	return $nombres; 
});
*/


//Route::controller('test', 'TestController');
Route::controller('auth', 'AuthController');

Route::controller('posts', 'PostsController');

Route::get('users/removephoto/{id}', 'UsersController@removephoto');
Route::get('users/delete/{id}', 'UsersController@delete');
Route::resource('users', 'UsersController');

Route::resource('molds', 'MoldsController');
Route::resource('usertype', 'UsertypeController');
Route::resource('lines', 'LinesController');

Route::controller('efficiency', 'EfficiencyController');

Route::controller('discussion', 'DiscussionController');

Route::resource('problemproduction', 'ProblemProductionController');

Route::get('models/delete/{id}', 'ModelsController@delete');
Route::resource('models', 'ModelsController');
Route::controller('production', 'ProductionController');
Route::controller('plan', 'PlanController');
Route::resource('shifts', 'ShiftsController');
Route::resource('defects', 'DefectsController');
Route::controller('defectiveProduction', 'DefectiveProductionController');


Route::get('efficiencyInsert/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}/{param7?}', function ($line, $model, $shift, $ct, $p_plan, $p_real, $comment) {
      
   $eff = new Efficiency();
   $eff->line_id = $line;
   $eff->model_id = $model;
   $eff->shift_id =  $shift;
   $eff->ct = $ct;
   $eff->production_plan = $p_plan;
   $eff->production_real = $p_real;
   $eff->comment = $comment;

   if ($eff->save()) {
      return $eff->id;
   }
   else{
      return 'NO';
   }
});
//-----

Route::get('planInsert/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}/{param7?}', function ($model_id, $line_id, $shift_id, $comment, $quantity, $date) {
      
   $user_id = Session::get('user-id');

   $plan = new Plan();
   $plan->user_id = $user_id;
   $plan->model_id = $model_id;
   $plan->line_id = $line_id;
   $plan->shift_id = $shift_id;
   $plan->production_date = $date;//date('Y-m-d H:s:i');
   $plan->comment = $comment;
   $plan->quantity = $quantity;
   $plan->status_data = 1;
   $plan->status_plan = 0;
   if($plan->save()){

   }else{

   }
   
});

Route::get('updatePlan/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}/{param7?}/{param8}', function ($id, $model_id, $line_id, $shift_id, $production_date, $comment, $quantity, $status_plan) {

   $plan = Plan::find($id);

   $plan->model_id = $model_id; //= Input::get('codigo');
   $plan->line_id = $line_id;
   $plan->shift_id = $shift_id;
   $plan->production_date = $production_date;
   $plan->comment = $comment;
   $plan->quantity = $quantity;
   $plan->status_plan = $status_plan;
      $plan->save();
      $plan = Plan::with('line', 'user', 'model.mold', 'shift')->where('production_date', $production_date)->get();
      return Redirect::to('plan')->with('notice', 'El molde ha sido modificado correctamente')->with('plan', $plan);
   //}
});


//Route::get('users/post/{id}', array('uses' => 'UsersController@post'));

//Route::post('users/post/{id}', array('uses' => 'UsersController@post'));

//Route::resource('users', 'UsersController', array('except' => array('post')));

//Route::get('users/', array('uses' => 'UsersController@index'));

//Route::controller('users','UsersController');

//Route::get('/users{id}/edit', 'UsersController@edit');
