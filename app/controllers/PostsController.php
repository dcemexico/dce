<?php
class PostsController extends BaseController {

   public function get_posts() {
      $posts = Post::orderBy('updated_at', 'desc')->with('user')->get();
      return $posts;
      //return View::make('shifts.index')->with('shifts', $shifts)->with('autoriz', $this->autorizado);
   }

   public function get_save($body) {
      $post = new Post();
      $user = Auth::user();

      $post->body = $body;
      $post->user_id = $user->id;

      if($post->save()){
      	return $user;
      }else{
      	return null;
      }
      //return View::make('shifts.index')->with('shifts', $shifts)->with('autoriz', $this->autorizado);
   }
}
?>