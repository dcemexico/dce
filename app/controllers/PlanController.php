<?php
class PlanController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::check() and Auth::user()->level == 5);
   }
   public function getIndex() {
      $plan = Plan::with('line', 'user', 'model.mold', 'shift')->where('production_date', date('Y-m-d'))->orderBy('line_id')->get();
      return View::make('plan.index')->with('plan', $plan);
   }
   public function getData($date) {
      $plan = Plan::with('line', 'user', 'model.mold', 'shift')->where('status_data', 1)->where('production_date', $date)->orderBy('line_id')->get();
      return $plan;
   }
   public function getDates($date) {
      $plan = Plan::with('line', 'user', 'model.mold', 'shift')->where('production_date', $date)->orderBy('line_id')->get();
      return $plan;
   }
   public function getForm() {
      $lines = Line::lists('lin_description', 'id');
      $molds = Mold::lists('mol_description', 'id');
      $shifts = Shift::lists('shi_description', 'id');
      return View::make('plan.formPlan')->with('lines', $lines)->with('molds', $molds)->with('shifts', $shifts);
   }

   public function getStatuschange($id) {
      $plan = Plan::find($id);
      $plan->status_data = 0;
      $plan->save();
      return "Change status!";
   }
   public function getModel ($id){
      $model = Model::where('mold_id', $id)->get();
      return $model;
   }

   public function getEdit($id){
      $plan = Plan::find($id);

      $lines = Line::lists('lin_description', 'id');
      $shifts = Shift::lists('shi_description', 'id');
      $molds = Mold::lists('codigo', 'id');
      $models = Model::lists('mod_description', 'id');
       $mold = Model::find($plan->model_id);
      return View::make('plan.save')->with('plan', $plan)->with('lines', $lines)->with('shifts', $shifts)->with('molds', $molds)->with('models', $models)->with('mold', $mold);
   }

   public function getDelete($id) {
      $plan = Plan::find($id);
      $plan->delete();
      return Redirect::to('plan')->with('notice', 'El plan se elimino');  
   }
}
?>