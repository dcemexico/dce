<?php
	class DefectiveProductionController extends BaseController {

		public function getIndex() {
   			$def_production = DefectiveProduction::with('model.mold', 'line', 'user', 'defect')->select('*' ,DB::raw('count(defect_id) as quantity'))->groupBy('line_id', 'model_id')->get();
			//$plan = Plan::with('line', 'user', 'model.mold', 'shift')->where('production_date', $date)->get();
			return $def_production;

			return View::make('production.index')->with('def_production', $def_production);
		}
		public function getLine() {
   			$production = Production::with('model', 'line', 'user', 'arduino')->select('*' ,DB::raw('count(line_id) as quantity'))->groupBy('line_id', 'model_id')->get();
			return $production;
		}
	}
?>