<?php
	class ProductionController extends BaseController {

		public function getIndex() {
   			$production = Production::with('model.mold', 'line', 'user', 'arduino')->select('*' ,DB::raw('count(line_id) as quantity'))->groupBy('line_id', 'plan_id', 'model_id')->where('created_at', 'LIKE', '%'.date('y-m-d').'%')->get();			
			return View::make('production.index')->with('production', $production);
		}
		public function getLine($date) {
   			$production = Production::with('model.mold', 'line', 'user', 'arduino')->select('*' ,DB::raw('count(line_id) as quantity'))->groupBy('line_id', 'plan_id', 'model_id')->where('created_at', 'LIKE', '%'.$date.'%')->get();
			return $production;
		}

	}
?>