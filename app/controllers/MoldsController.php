<?php
class MoldsController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
   public function index() {
      $molds = Mold::all();
      return View::make('molds.index')->with('molds', $molds)->with('autoriz', $this->autorizado);
   }

   public function show($id) {
      /*$mold = Mold::find($id);
      return View::make('molds.show')->with('mold', $mold);//->with('autoriz', $this->autorizado);*/
   }
   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $mold = new Mold();
      return View::make('molds.save')->with('mold',$mold);
   }

   public function store() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $mold = new Mold();
      $mold->codigo = Input::get('codigo');
      $mold->mol_description = Input::get('mol_description');

      
      $validator = Mold::validate(array(
         'codigo' => Input::get('codigo'),
         'mol_description' => Input::get('mol_description'),
      ));

      if ($validator->fails()) {
         $errors = $validator->messages()->all();
        
         return View::make('molds.save')->with('mold', $mold)->with('errors', $errors);
      }else{
         $mold->save();
         return Redirect::to('molds')->with('notice', 'El molde ha sido creado correctamente');//////////////////////////////
      }
   }
   public function edit($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $mold = Mold::find($id);
      return View::make('molds.save')->with('mold', $mold);
   }

   public function update($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $mold = Mold::find($id);
      $mold->codigo = Input::get('codigo');
      $mold->mol_description = Input::get('mol_description');
      
      $validator = Mold::validate(array(
         'codigo' => Input::get('codigo'),
         'mol_description' => Input::get('mol_description'),
      ), $mold->id);

      if($validator->fails()){
         $errors = $validator->messages()->all();
         return View::make('molds.save')->with('mold', $mold)->with('errors', $errors);
      }else{
         $mold->save();
         return Redirect::to('molds')->with('notice', 'El molde ha sido modificado correctamente');////////////////
      }
   }
   public function destroy($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $mold = Mold::find($id);
      $mold->delete();
      return Redirect::to('molds')->with('notice', 'El molde se elimino');  
   }
}
?>