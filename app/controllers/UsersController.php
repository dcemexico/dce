<?php
class UsersController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
    public function index() {
      $user = Auth::user();
      Session::put('name-photo', $user->photographic);
      $users = User::with('usertype')->get();
      return View::make('users.index')->with('users', $users)->with('autoriz', $this->autorizado);
   }

   public function show($id) {
      /*$user = User::find($id);
      return View::make('users.show')->with('user', $user)->with('autoriz', $this->autorizado);*/
   }
   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $user = new User();
      $usertypes = UserType::lists('ut_description', 'id');
      return View::make('users.save')->with('user',$user)->with('autoriz', $this->autorizado)->with('usertypes', $usertypes);
   }
   public function store() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $user = new User();
      $user->nick = Input::get('nick');
      $user->name = Input::get('name');
      $user->email = Input::get('email');
      $user->password = Hash::make(Input::get('password'));
      $user->usertype_id =Input::get('usertype_id');
      $user->active = true;

      $validator = User::validate(array(
         'name' => Input::get('name'),
         'email' => Input::get('email'),
         'password' => Input::get('password'),
         'usertype_id' => Input::get('usertype_id'),
      ));

      if ($validator->fails()) {
         $errors = $validator->messages()->all();
         $user->password = null;
         return View::make('users.save')->with('user', $user)->with('errors', $errors)->with('autoriz', $this->autorizado);
      }else{
          $user->save();
         return Redirect::to('users')->with('notice', 'El usuario ha sido creado correctamente'); ////////////////////////////////////
      }
   }
   public function edit($id) { 
      if(!$this->autorizado){
         if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      }
      else{ 
         $user = User::find($id);
         $usertypes = UserType::lists('ut_description', 'id');
      }
      return View::make('users.save')->with('user', $user)->with('autoriz', $this->autorizado)->with('usertypes', $usertypes);
   }

   public function update($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $user = User::find($id);
      $user->nick = Input::get('nick');
      $user->name = Input::get('name');
      $user->email = Input::get('email');
      $user->usertype_id = Input::get('usertype_id');

      $destinationPath = '';
      $filename = '';

      if (Input::hasFile('photographic')) {
         $file = Input::file('photographic');
         $destinationPath = public_path().'/images/users/';
         $filename = str_random(6).'_'.$file->getClientOriginalName();
         $uploadSuccess = $file->move($destinationPath, $filename); 
         $user->photographic = $filename;
      }

      $validator = User::validate(array(
         'name' => Input::get('name'),
         'email' => Input::get('email'),
         'password' => $user->password,
         'usertype_id' => Input::get('usertype_id'), 
      ), $user->id);

      $usertypes = UserType::lists('ut_description', 'id');
      if($validator->fails()){
         $errors = $validator->messages()->all();
         $user->password = null;
         return View::make('users.save')->with('user', $user)->with('errors', $errors)->with('usertypes', $usertypes);
      }else{
         $user->save();
         return Redirect::to('users')->with('notice', 'El usuario ha sido modificado correctamente.')->with('autoriz', $this->autorizado)->with('usertypes', $usertypes);
      }
   }
   public function delete ($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $value = Session::get('user-id');
      if ($value == $id) {
         return '2';
      }else{
         $user = User::find($id);
         if($user->delete()){
            return '0';
         }else{
            return '1';
         }
      }
   }
   public function removephoto ($user_id){
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $user = User::find($user_id);
      $user->photographic = '';
      if ($user->save()) {
         return '0';
      }else{
         return '1';
      }
      
   }
}
?>