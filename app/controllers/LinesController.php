<?php
class LinesController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
   public function index() {
      
      $lines = Line::all();
      return View::make('lines.index')->with('lines', $lines)->with('autoriz', $this->autorizado);
   }

   public function show($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $line = Line::find($id);
      return View::make('lines.show')->with('line', $line);
   }
   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $line = new Line();
      return View::make('lines.save')->with('line',$line);
   }

   public function store() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $line = new Line();
      $line->lin_description = Input::get('lin_description');

      
      $validator = Line::validate(array(
         'lin_description' => Input::get('lin_description'),
      ));

      if ($validator->fails()) {
         $errors = $validator->messages()->all();
         return View::make('lines.save')->with('line', $line)->with('errors', $errors);
      }else{
         $line->save();
         return Redirect::to('lines')->with('notice', 'La linea ha sido creada correctamente');
      }
   }
   public function edit($id) { 
     if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $line = Line::find($id);
      return View::make('lines.save')->with('line', $line);//->with;('autoriz', $this->autorizado);
   }

   public function update($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $line = Line::find($id);
      $line->lin_description = Input::get('lin_description');
      
      $validator = Line::validate(array(
         'lin_description' => Input::get('lin_description'),
      ), $line->id);
   
      if($validator->fails()){
         $errors = $validator->messages()->all();
         return View::make('lines.save')->with('line', $line)->with('errors', $errors);
      }else{
         $line->save();
         return Redirect::to('lines')->with('notice', 'La linea ha sido modificada correctamente');
      }
   }
   public function destroy($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $line = Line::find($id);
      $line->delete();
      return Redirect::to('lines')->with('notice', 'La linea se elimino');  
   }
}
?>