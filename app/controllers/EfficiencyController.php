<?php
class EfficiencyController extends BaseController {

   public function get_index() {
      $efficiency = Efficiency::with('line')->orderBy('id', 'asc')->with('shift')->with('model')->where('created_at', 'LIKE', '%'.date('y-m-d').'%')->get();
      $defects =  Defect::orderBy('problemproduction_id', 'asc')->get();
      $date = date('y-m-d');
      return View::make('efficiency.index')->with('efficiency', $efficiency)->with('defects', $defects)->with('date', $date);
   }

   public function get_search() {
      return $date = Input::get('input_date');
      $efficiency = Efficiency::with('line')->orderBy('id', 'asc')->with('shift')->with('model')->where('created_at', 'LIKE', '%'.$date.'%')->get();
      $defects =  Defect::orderBy('problemproduction_id', 'asc')->get();
      return View::make('efficiency.index')->with('efficiency', $efficiency)->with('defects', $defects)->with('date', $date);
   }

   public function get_edit($id) {
      $lines = Line::lists('lin_description', 'id');
      $molds = Mold::lists('mol_description', 'id');
      $shifts = Shift::lists('shi_description', 'id');

      $efficiency = Efficiency::find($id);
      $models = Model::lists('mod_description', 'id');
      
      $mold = Model::find($efficiency->model_id);

      $defects =  EfficiencyDefects::where('efficiency_id', $efficiency->id)->with('defect')->get();
      return View::make('efficiency.save')->with('efficiency', $efficiency)->with('lines', $lines)->with('models', $models)->with('molds', $molds)->with('shifts', $shifts)->with('mold', $mold)->with('defects', $defects);
   }

   public function get_store() {
      $id_eff = Input::get('id_eff');
      $model_id = Input::get('model');
      $shift_id = Input::get('shift');
      $ct = Input::get('ct');
      $production_plan = Input::get('pp');
      $production_real = Input::get('pr');
      $comment = Input::get('comment');
      $line = Input::get('line');

      $efficiency = Efficiency::find($id_eff);
      
      $efficiency->model_id = $model_id;
      $efficiency->line_id = $line;
      $efficiency->shift_id = $shift_id;
      $efficiency->CT = $ct;
      $efficiency->production_plan = $production_plan;
      $efficiency->production_real = $production_real;
      $efficiency->comment = $comment;

      if ($efficiency->save()) {
         return '0';
      }else{
         return '1';
      }
      //$line = Input::get('line');
   }

   public function get_form() {

      $lines = Line::lists('lin_description', 'id');
      $molds = Mold::lists('mol_description', 'id');
      $shifts = Shift::lists('shi_description', 'id');

      return View::make('efficiency.formEfficiency')->with('lines', $lines)->with('molds', $molds)->with('shifts', $shifts);
   }

   public function get_problems($key) {
      return $defects = Defect::where('name','LIKE',  '%'.$key.'%')->get();
   }

   public function get_insertp($id_eff, $id_problem, $qua) {
      $dp = new EfficiencyDefects();

      $dp->efficiency_id = $id_eff;
      $dp->defect_id = $id_problem;
      $dp->quantity = $qua;

      if ($dp->save()) {
         return 'SI';
      }else{
         return 'NO';
      }
      return ;
   }

   public function get_editpro($id_eff, $id_problem, $qua) {
      $dp = new EfficiencyDefects();

      $dp->efficiency_id = $id_eff;
      $dp->defect_id = $id_problem;
      $dp->quantity = $qua;

      if ($dp->save()) {
         return 'SI';
      }else{
         return 'NO';
      }
      return ;
   }
}
?>