<?php
class ModelsController extends BaseController {

   private $autorizado;

   public function __construct(){
      $this->autorizado = (Auth::user()->usertype_id == 1);
   }
   public function index() {
      $models = Model::with('mold')->get();
      return View::make('models.index')->with('models', $models)->with('autoriz', $this->autorizado);
   }

   public function show($id) {
      $model = Model::with('mold')->where('id', $id)->first();
      return View::make('models.show')->with('model', $model);
   }

   public function create() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $model = new Model();
      $molds = Mold::lists('codigo', 'id');
      return View::make('models.save')->with('model',$model)->with('molds', $molds);
   }

   public function store() {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $model = new Model();
      $model->codigo = Input::get('codigo');
      $model->mod_description = Input::get('mod_description');
      $model->mold_id = Input::get('mold_id');
      $molds = Mold::lists('codigo', 'id');
      $validator = Model::validate(array(
         'codigo' => Input::get('codigo'),
         'mod_description' => Input::get('mod_description'),
         'mold_id' => Input::get('mold_id'), 
      ));

      if($validator->fails()){
         $errors = $validator->messages()->all();
         return View::make('models.save')->with('model', $model)->with('errors', $errors)->with('molds', $molds);
      } else {
         $model->save();
            return Redirect::to('models')->with('notice', 'El modelo ha sido creado correctamente');////////////////////////
      }
   }
   public function edit($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $model = Model::find($id);
      $molds = Mold::lists('codigo', 'id');
      return View::make('models.save')->with('model', $model)->with('molds', $molds);
   }

   public function update($id) { 
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $model = Model::find($id);
      $model->codigo = Input::get('codigo');
      $model->mod_description = Input::get('mod_description');
      $model->mold_id = Input::get('mold_id');
      $molds = Mold::lists('codigo', 'id');
      $validator = Model::validate(array(
         'codigo' => Input::get('codigo'),
         'mod_description' => Input::get('mod_description'),
         'mold_id' => Input::get('mold_id'),
      ), $model->id);
   
      if($validator->fails()){

         $errors = $validator->messages()->all();
         return View::make('models.save')->with('model', $model)->with('errors', $errors)->with('molds', $molds);
      }else{
         $model->save();
         return Redirect::to('models')->with('notice', 'El modelo ha sido modificado correctamente');///////////////
      }
   }
   public function delete($id) {
      if(!$this->autorizado) return Redirect::to('/auth/panel')->with('notice', "You can't do it!");
      $model = Model::find($id);
      if($model->delete()){
         return '0';
      }else{
         return '1';
      }
   }
}
?>