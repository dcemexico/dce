@extends('layout.master')
{{ HTML::style('css/users.css') }}

@section('content')
{{ HTML::script('js/user-delete.js')}}
    @if(Session::has('notice'))
    	 <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    @if($autoriz)
    <p> 
        {{ HTML::image('images/icos/user.png', 'Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('users/create', 'Create new user') }} 
    </p>
    @endif    
        <div class="content-data" id="content-production-users">
        <div class="title-content-data">List of Users </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Photo </th>
                        <th> Nombre real </th>
                        <th> Nick </th>
                        <th> Email </th>
                        <th> Nivel </th>
                        <th> Estado </th>
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count())
                        @foreach($users as $item)
                        <tr class="user-data-{{$item->id}}">
                            <td>
                                @if($item->photographic)
                                    <img src="images/users/{{  $item->photographic }}" class="user-photo" />
                                @else
                                    <img src="images/users/nophoto.png" class="user-photo" />
                                @endif
                            </td>
                            <td> {{ $item->name }}</td>
                            <td> {{ $item->nick }}</td>
                            <td> {{ $item->email }} </td>
                            <td> {{ $item->usertype->name }} </td>
                            <td> {{ ($item->active) ? 'si' : 'no' }}</td>
                            @if($autoriz)
                            <td> 
                                <a href="users/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                                <a class="user_delete">
                                    <input id="user_id" type="hidden" value="{{$item->id}}" />
                                    <img src="images/icos/delete.png" class="imageIcos">
                                </a> 
                            </td>
                             @endif
                        </tr>
                        @endforeach
                    @else
                        <p>no se encontro datos</p>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop
