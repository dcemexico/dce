@extends('layout.master')
@section('content')
{{ HTML::style('css/user-update.css')}}
{{ HTML::script('js/jquery-ui.js') }}
{{ HTML::script('js/user-save.js') }}
  
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
 <div class="content-data" id="">
  <div class="panel-data-edit">

  {{ Form::open(array('url' => 'users/' . $user->id, 'files' =>true)) }}
  	{{ Form::label ('name', 'Name') }}
     	<br />
      {{ Form::hidden ('user_id', $user->id, array('id'=>'user_id')) }}

     	{{ Form::text ('name', $user->name) }}
     	<br />
     	{{ Form::label ('email', 'Email') }}
     	<br />
     	{{ Form::text ('email', $user->email) }} 

      <br />
      {{ Form::label ('nick', 'Nick') }}
      <br />
      {{ Form::text ('nick', $user->nick) }} 
     	<br /> 
     	@if($user->id)
      	  {{ Form::hidden ('_method', 'PUT') }}
   	  @else
     		{{ Form::label ('password', 'Contraseña') }}
      	<br />
      	{{ Form::password ('password') }}
     		<br />
     	@endif
      {{ Form::label ('usertype_id', 'Type') }}
      <br />
      {{ Form::select('usertype_id', $usertypes, array('selected' => $user->usertype_id))}}
     	<br />
      @if($user->photographic)
          <img src="../../images/users/{{  $user->photographic }}" class="image-user" id="user-photo" />
          <br />
          <a id="delete-user-photo" class="photo-delete">Delete photo</a>
      @else
          <img src="../../images/users/nophoto.png" class="image-user" id="user-photo" />
      @endif
      <br />
      {{ Form::file('photographic')}}
      <br />
     	{{ Form::submit('Save') }}
     	{{ link_to('users', 'Cancel') }}
  {{ Form::close() }}
  
  </div>
  </div>
@stop