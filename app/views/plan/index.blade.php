@extends('layout.master')
<div class="modal" id="modal">
</div>
@section('content')
{{ HTML::script('js/data_plan.js') }}
{{ HTML::script('js/jquery-ui.js') }}
{{ HTML::script('js/jquery.tablesorter.min.js') }}


{{ HTML::style('css/plan.css') }}
{{ HTML::style('css/modal.css') }}
{{ HTML::style('http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css')}}


    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        {{ HTML::image('images/icos/plan.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        <a href="#add-plan" id="add_plan">Add new plan</a>
        
        <span class="fechaPlan"> {{ HTML::image('images/icos/dateGreen.png', 'Imagen not found', array('class' => 'imageIcos'))}} 
        <a id="plan-date" class="plan-date">Plan Date</a> 
        <input type="text" id="plans" /></span>
    </p>    
        <div class="content-data" id="content-production-molds">
        <!--<div class="title-content-data">Plan </div>-->
        <div>
            <table id="table-line">
                <thead>
                    <tr>
                        <th> Line </th>
                        <th> Model </th>
                        <!--<th> Mold </th>-->
                        <th> Turno</th>
                        <th> No. Order</th>
                        <th> Date</th>
                        <th> Comment</th>
                        <th id="content-production-molds-th-comment"> Quantity</th>
                        <th> Status </th>
                    </tr>
                </thead>
                <tbody id="data_plan">
                    @if($plan->count())
                        @foreach($plan as $item)
                         <tr>
                            @if($item->line_id > 0)
                            <td> {{ $item->line->lin_description }} </td>
                             @else
                            <td></td>
                            @endif


                            @if($item->model_id > 0)
                                <td> {{ $item->model->codigo }} </td>
                            @else
                                <td></td>
                            @endif

                            @if($item->model_id > 0)
                                @if(!is_null($item->model->mold))
                                    <!--<td> {{ $item->model->mold->codigo }} </td>-->
                                @else
                                    <!--<td></td>-->
                                @endif
                            @else
                                <!--<td></td>-->
                            @endif
                            
                            @if(!is_null($item->shift_id))
                                <td> {{ $item->shift->shi_description }} </td>
                            @else
                                <td></td>
                            @endif
                            
                            
                            <td> {{ $item->order }} </td>
                            <td> {{ $item->production_date }} </td>
                            @if(!is_null($item->comment))
                                <td> {{ $item->comment }} </td>
                            @else
                                <td></td>
                            @endif
                            
                            <td id="content-production-molds-td-comment"> {{ $item->quantity }} </td>
                            <td> <img src="images/icos/{{$item->status_plan}}.png" class="imageIcos" /></td>
                            <td> {{ link_to('plan/edit/'.$item->id, 'Edit') }} </td>
                        </tr>
                        @endforeach
                    @else
                    @endif
                </tbody>
            </table>
        </div>
    </div>

   <script type="text/javascript">
    $(function () {
        $('#table-line').tablesorter({
            sortList: [[0,0],[2,0]] 
        });
    });
   </script>

    @stop
