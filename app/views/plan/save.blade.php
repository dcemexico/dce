@extends('layout.master')
@section('content')
  {{ HTML::style('css/plan-save.css') }}
  
  {{ HTML::script('js/jquery-ui.js') }}
  {{ HTML::script('js/plan-save.js') }}
  {{ HTML::style('http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css')}}

  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif

  @if($plan->count())
 <div class="content-data" id="">
  <div class="panel-data-edit">

  {{ Form::open(array('url' => '')) }}

      {{ Form::hidden('plan_id', $plan->id,  array('id'=>'plan_id'))}}

  	  {{ Form::label ('line_id', 'Line') }}
     	<br />
      {{ Form::select('line_id', $lines, array('selected' => $plan->line_id),  array('id' => 'line_id') )}}
      <br />

      {{ Form::label ('mold_id', 'Mold') }}
      <br />
      


      {{ Form::select('mold_id', array('0'=> '-- Select --')+$molds, array('selected' => $plan->mold_id) )}}
      <br />

      {{ Form::label ('model_id', 'Model') }}
      <br />
      <div>
        <?php
        if($plan->model_id < 1)
          $models = [];
        ?>
        {{ Form::select('model_id', array('0'=> '-- Select --') + $models, array('selected' => $plan->model_id), array('id' => 'model_id')) }}
      </div>
      <br />

      {{ Form::label ('shift_id', 'Shift') }}
      <br />
      {{ Form::select('shift_id', $shifts, array('selected' => $plan->shift_id))}}
      <br />

      {{ Form::label ('date_plan', 'Date') }}
      <br />
      {{ Form::text('date_plan', $plan->production_date, array('id'=>'date_plan')) }}
      <br />
      {{ Form::label ('quantity', 'Quantity') }}
      <br />
      {{ Form::text('quantity',  $plan->quantity, array('id'=>'quantity')) }}

      <br />
      {{ Form::label ('comment', 'Comment') }}
      <br />
      {{ Form::textarea('comment', $plan->comment,array('id'=>'comment')) }}

      <br />
      <div class="status_plan">
        {{ Form::label('status_plan', 'Active')}}
        {{ Form::radio('status_plan', '1', ($plan->status_plan == '1') ? true : false, array('id'=> 'rb_status_plan')) }}

        {{ Form::label('status_plan', 'Desactive')}}
        {{ Form::radio('status_plan', '2', ($plan->status_plan == '2') ? true : false, array('id'=>'rb_status_plan'))}}
      </div>
      <hr />
     	
  {{ Form::close() }}
  <button id="edit-plan">Save plan</button> {{ link_to('plan', 'Cancel') }}
  <u> {{ link_to('plan/delete/'.$plan->id, 'Delete plan') }} </u>
  </div>
  </div>
   @endif
@stop