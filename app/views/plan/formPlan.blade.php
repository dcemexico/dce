<header>Plan
     <span class="close-modal" id="close-modal">
        {{ HTML::image('images/icos/close-content.png', 'Imagen not found' , array('class'=> 'imageIcos')) }}
     </span>  
</header>

{{ Form::open(array('url' => 'plan/', 'id' => 'formPlan')) }}

	{{ Form::label ('line_id', 'Line',  array('multiple' => true)) }}
	{{ Form::select ('line_id', array('0'=> ' -- Please select -- ') + $lines) }}

	{{ Form::label ('mold_id', 'Mold',  array('multiple' => true)) }}
	{{ Form::select ('mold_id', array('0' => ' -- Please select -- ') + $molds, 'default', array('id' => 'mold_id')) }}

	{{ Form::label ('model_id', 'Model',  array('multiple' => true)) }}
	<div>
		{{ Form::select ('model_id', array('0' => ' -- Please select -- '), null, array('id' => 'model_id')) }}
	</div>

	{{ Form::label ('shift_id', 'Shift',  array('multiple' => true)) }}
	{{ Form::select ('shift_id', array('0' => ' -- Please select -- ') + $shifts) }}

	{{ Form::label ('date_plan') }}
	{{ Form::text('date_plan', '', array('id'=>'date_plan')) }}

	{{ Form::label ('quantity', 'Quantity') }}
	{{ Form::text('quantity', '', array('id'=>'quantity')) }}

	{{ Form::label ('comment', 'Comment') }}
	{{ Form::textarea('comment', '', array('id'=>'comment')) }}

{{ Form::close() }}
<button id="insert-plan">Add plan</button>
<span id="ressult-modal" style="display:none"></span>