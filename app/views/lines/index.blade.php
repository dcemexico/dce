@extends('layout.master')
{{ HTML::style('css/molds.css') }}
@section('content')

    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        {{ HTML::image('images/icos/mold.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('lines/create', ' Create new line ') }} 
    </p>

    @if($lines->count())
        <div class="content-data" id="content-production-lines">
        <div class="title-content-data">List of Lines </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Code (Id) </th>
                        <th> Description </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lines as $item)
                    <tr>
                        <td> {{ $item->id }} </td>
                        <td> {{ $item->lin_description }} </td>
                        @if($autoriz)
                            <td> 
                                <a href="lines/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                                <a class="line_delete">
                                    <input id="line_id" type="hidden" value="{{$item->id}}" />
                                    <img src="images/icos/delete.png" class="imageIcos">
                                </a> 
                            </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

        


    @else
        <p>no se encontro datos</p>
    @endif

    @stop
