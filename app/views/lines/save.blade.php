@extends('layout.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
<div class="content-data" id="">
  <div class="panel-data-edit">
  {{ Form::open(array('url' => 'lines/' . $line->id)) }}

      @if($line->id)
          {{ Form::hidden ('_method', 'PUT') }}
      @endif

      {{ Form::label ('lin_description', 'Description') }}
      <br />
      {{ Form::textarea ('lin_description', $line->lin_description) }}
      <br />

     	{{ Form::submit('Save') }}
     	{{ link_to('lines', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop