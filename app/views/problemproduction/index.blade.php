@extends('layout.master')

@section('content')

    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        {{ HTML::image('images/icos/mold.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('problemproduction/create', ' Create new type problem ') }} 
    </p>

    
        <div class="content-data" id="content-production-problemproduction">
        <div class="title-content-data">List of problem production</div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Color </th>
                        <th> Name </th>
                        <th> Description </th>
                        <!--<th> </th>-->
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @if($pp->count())
                    @foreach($pp as $item)
                    <tr>
                        <td> {{ $item->color_identify }} <span style="position:absolute;width:40px;background-color:{{ $item->color_identify }}"> &nbsp; </span> </td>                     
                        <td> {{ $item->name }} </td>
                        <td> {{ $item->pp_description }} </td>
                        @if($autoriz)
                            <td> 
                                <a href="problemproduction/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                                <a class="problemproduction_delete">
                                    <input id="pp_id" type="hidden" value="{{$item->id}}" />
                                    <img src="images/icos/delete.png" class="imageIcos">
                                </a> 
                            </td>
                        @endif
                    </tr>
                    @endforeach
                    @else
                        <p>no se encontro datos</p>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    

    @stop
