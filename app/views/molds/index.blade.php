@extends('layout.master')
{{ HTML::style('css/molds.css') }}
@section('content')

    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        {{ HTML::image('images/icos/mold.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('molds/create', ' Create new mold ') }} 
    </p>

    @if($molds->count())
        <div class="content-data" id="content-production-molds">
        <div class="title-content-data">List of Molds </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Code </th>
                        <th> Description </th>
                        <!--<th> </th>-->
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($molds as $item)
                    <tr>
                        <td>{{ $item->codigo }}</td>
                        <td> {{ $item->mol_description }} </td>
                        @if($autoriz)
                        <td> 
                            <a href="molds/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                            <a class="mold_delete">
                                <input id="mold_id" type="hidden" value="{{$item->id}}" />
                                <img src="images/icos/delete.png" class="imageIcos"></a>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

        


    @else
        <p>no se encontro datos</p>
    @endif

    @stop
