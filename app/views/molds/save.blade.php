@extends('layout.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
 <div class="content-data" id="">
  <div class="panel-data-edit">
  {{ Form::open(array('url' => 'molds/' . $mold->id)) }}
  	{{ Form::label ('codigo', 'Codigo') }}

      @if($mold->id)
          {{ Form::hidden ('_method', 'PUT') }}
      @endif

     	<br />
     	{{ Form::text ('codigo', $mold->codigo) }}
     	<br />

      {{ Form::label ('mol_description', 'Description') }}
      <br />
      {{ Form::textarea ('mol_description', $mold->mol_description) }}
      <br />

     	{{ Form::submit('Save') }}
     	{{ link_to('molds', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop