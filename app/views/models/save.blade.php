@extends('layout.master')
@section('content')
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
 <div class="content-data" id="">
  <div class="panel-data-edit">
  {{ Form::open(array('url' => 'models/' . $model->id)) }}

      @if($model->id)
          {{ Form::hidden ('_method', 'PUT') }}
      @endif

    	{{ Form::label ('codigo', 'Codigo') }}
     	<br />
     	{{ Form::text ('codigo', $model->codigo) }}
     	<br />
      {{ Form::label ('mol_description', 'Description') }}
      <br />
      {{ Form::textarea ('mod_description', $model->mod_description) }}
      <br />
      {{ Form::label ('mold_id', 'Codigo Mold') }}
      <br />
      {{ Form::select('mold_id', array('0'=> '-- Select --')+$molds, array('selected' => $model->mold_id) )}}
      <br />

     	{{ Form::submit('Save') }}
     	
      {{ link_to('models', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop