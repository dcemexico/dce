@extends('layout.master')
@section('content')
{{ HTML::script('js/model-delete.js')}}
    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        {{ HTML::image('images/icos/model.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('models/create', ' Create new model') }} 
    </p>

    
        <div class="content-data" id="content-production-molds">
        <div class="title-content-data">List of Models </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Model </th>
                        <th> Description model </th>
                        <th> Mold</th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @if($models->count())
                        @foreach($models as $item)
                    <tr class="model-data-{{$item->id}}">
                        <td>{{ $item->codigo }}</td>
                        <td> {{ $item->mod_description }} </td>
                        @if($item->mold_id > 0)
                            <td> {{ $item->mold->codigo }} </td>
                        @else
                            <td></td>
                        @endif
                        
                        <td> {{-- link_to('models/'.$item->id, 'Show') --}} </td>
                        @if($autoriz)
                        <td> 
                            <a href="models/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                            <a class="model_delete">
                                <input id="model_id" type="hidden" value="{{$item->id}}" />
                                <img src="images/icos/delete.png" class="imageIcos"></a>
                        </td>
                        @endif
                    </tr>
                        @endforeach
                    @else

                    @endif

                    
                </tbody>
            </table>
        </div>
    </div>
    

    @stop
