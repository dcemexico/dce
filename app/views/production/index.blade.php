@extends('layout.master')
{{ HTML::style('css/production.css') }}

@section('content')
{{ HTML::script('js/data_production_line.js') }}
{{ HTML::script('js/jquery-ui.js') }}

{{ HTML::style('http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css')}}

    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        <span class="fechaPlan"> {{ HTML::image('images/icos/dateGreen.png', 'Imagen not found', array('class' => 'imageIcos'))}} 
        <a id="" class="">Plan Date</a> 
        <input type="text" id="production_datepicker" /></span>
    </p>
    <div class="content-data" id="content-production-total">
        <div class="title-content-data">Production </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Line </th>
                        <th> Quantity </th>
                        <th> Model </th>
                        <th> Device </th>
                        <th> With Plan</th>
                    </tr>
                </thead>
                <tbody id="data_production_line">
                    @foreach($production as $item)
                        <tr>

                            @if(!is_null($item->line->lin_description))
                                <td> {{ $item->line->lin_description }} </td>
                            @else
                                <td></td>
                            @endif

                            @if(!is_null($item->quantity))
                                <td> {{ $item->quantity }} </td>
                            @else
                                <td></td>
                            @endif

                            @if(!is_null($item->model_id))
                                <td> {{ $item->model->mod_description }} </td>
                            @else
                                <td></td>
                            @endif
                            
                            <td> {{ $item->arduino->ard_description }}</td>
                            @if(!is_null($item->plan_id))
                                <td> YES </td>
                            @else
                                <td>NO</td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@stop