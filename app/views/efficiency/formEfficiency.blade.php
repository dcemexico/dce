<header>Efficiency
     <span class="close-modal" id="close-modal">
        {{ HTML::image('images/icos/close-content.png', 'Imagen not found' , array('class'=> 'imageIcos')) }}
     </span>  
</header>

	{{ Form::label ('line_id', 'Line',  array('multiple' => true)) }}
	{{ Form::select ('line_id', array('0'=> ' -- Please select -- ') + $lines) }}

	{{ Form::label ('mold_id', 'Mold',  array('multiple' => true)) }}
	{{ Form::select ('mold_id', array('0' => ' -- Please select -- ') + $molds, 'default', array('id' => 'mold_id')) }}

	{{ Form::label ('model_id', 'Model',  array('multiple' => true)) }}
	<div>
		{{ Form::select ('model_id', array('0' => ' -- Please select -- '), null, array('id' => 'model_id')) }}
	</div>

	{{ Form::label ('shift_id', 'Shift',  array('multiple' => true)) }}
	{{ Form::select ('shift_id', array('0' => ' -- Please select -- ') + $shifts) }}

	{{ Form::label ('CT') }}
	{{ Form::text('ct', '', array('id'=>'ct')) }}

	{{ Form::label ('Production Plan') }}
	{{ Form::text('production_plan', '', array('id'=>'production_plan')) }}

	{{ Form::label ('Production Real') }}
	{{ Form::text('production_real', '', array('id'=>'production_real')) }}

	{{ Form::label ('comment', 'Comment') }}
	{{ Form::textarea('comment', '', array('id'=>'comment')) }}

	<hr />

	{{ Form::label ('Search Problem') }}
	{{ Form::text('search_problem', '', array('id'=>'search_problem')) }}

	<div id="result-search-problem" class="result-search-problem">
		...
	</div>
	<hr />
		<div id="result-problem-efficiency">
			
		</div>
	<hr />

<button id="insert-efficiency">Add Efficiency</button>
<span id="ressult-modal" style="display:none"></span>