@extends('layout.master')
<div class="modal" id="modal">

</div>
@section('content')
{{ HTML::script('js/jquery-ui.js') }}
{{ HTML::style('http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css')}}

{{ HTML::style('css/modal.css') }}
{{ HTML::style('css/efficiency.css') }}
{{ HTML::script('js/efficiency.js') }}
{{ HTML::script('js/datepicker-efficiency.js') }}

    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
        <div class="content-data" id="content-production-efficiency">
                
            {{ Form::open(array('url' => '/efficiency/search')) }}
                {{ HTML::image('images/icos/date-black.png', 'Imagen not found', array('class'=>'imageIcos')) }}
                {{ Form::text ('input_date',$date,array('id'=>'input_date')) }}
                {{ Form::submit('Search') }}
                <a id="add_efficiency">{{ HTML::image('images/icos/add.png', 'Imagen not found', array('class'=>'imageIcos')) }} </a>
            {{ Form::close() }}
        <div>
        </div>
        <div>
            <table>
            <thead>
                <tr >
                    
                    <th rowspan="2">Code</th>
                    <th rowspan="2">Line</th>
                    <th rowspan="2">Shift</th>
                    <th rowspan="2">C/T</th>
                    <th rowspan="2">Plan </th>
                    <th rowspan="2">Real</th>
                    
                    <!--<th colspan="14" class="title-stoppage"><span>Stoppage</th>
                    <th colspan="8" class="title-defects">Defects</th>
                    <th colspan="8" class="title-scrap">Scrap</th>
                    <th colspan="8" class="efficiency">Efficiency</th>-->
                </tr>
                <tr>
                    @if($defects->count())
                        @foreach($defects as $item)
                            <th style="background-color:{{$item->problem_production->color_identify}};color:#fff;">{{$item->name; }}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if($efficiency->count())
                    @foreach($efficiency as $item)
                        <tr>

                            <td style="width:"> <a href="efficiency/edit/{{$item->id}}"> {{ $item->model->codigo }} </a></td>
                            <td style="width:">{{ $item->line->lin_description }}</td>
                            <td style="width:">{{ $item->shift->shi_description }}</td>
                            <td style="width:">{{ $item->CT }}</td>
                            <td style="width:">{{ $item->production_plan }}</td>
                            <td style="width:">{{ $item->production_real }}</td>

                            <?php
                                //Exception error!
                                $ed = EfficiencyDefects::with('defect')->where('efficiency_id', $item->id)->get();
                            ?>
                                @if($ed->count() and $defects->count())
                                   <?php $defect_array_ed_di = array()?>
                                   <?php $defect_array_ed_q = array()?>
                                   <?php $i = 0;?>

                                    @foreach($ed as $item_ed)
                                        <?php $defect_array_ed_di[$i] = $item_ed->defect_id?>
                                        <?php $defect_array_ed_q[$i] = $item_ed->quantity?>
                                        <?php $i++;?>
                                    @endforeach

                                    <?php $e = 0;?>
                                    @foreach($defects as $item_defect)
                                        @if(in_array($item_defect->id,$defect_array_ed_di))
                                            <td class="value">{{$defect_array_ed_q[$e]}}</td>
                                            <?php $e++;?>
                                        @else
                                            <td style="width:"> 0</td>
                                        @endif
                                        
                                    @endforeach
                                @endif
                                <?php
                                    $time = explode(" ", $item->created_at);
                                ?>
                                <td style="width:">{{ $time[1] }}</td>
                        </tr>
                    @endforeach
                @else
                <tr><td colspan="6"><span class="datanotfound">Data not found</span></td></tr>
                    
                @endif
            </tbody>
        </table>
    </div>
</div>
@stop
