@extends('layout.master')
@section('content')
{{ HTML::script('js/efficiency-save.js') }}

{{ HTML::style('css/efficiency.css') }}
  @if(isset($errors))
	   <ul>
	      @foreach($errors as $item)
	         <li> {{ $item }} </li>
	      @endforeach
	   </ul>
	@endif
  <div class="content-data" id="">
  <div class="panel-data-edit">
{{ Form::open(array('onsubmit' => 'return false','url' => 'efficiency/' . $efficiency->id)) }}
  {{ Form::hidden('efficiency_id', $efficiency->id, array('id'=>'efficiency_id')) }}

  <br />
  {{ Form::label ('line_id', 'Line',  array('multiple' => true)) }}
  <br />
  {{ Form::select ('line_id', array('0'=> ' -- Please select -- ') + $lines, $efficiency->line_id, array('id' => 'line_id')) }}
  <br />
  {{ Form::label ('mold_id', 'Mold',  array('multiple' => true)) }}
  <br />
  {{ Form::select ('mold_id', array('0' => ' -- Please select -- ') + $molds, $mold->mold_id, array('id' => 'mold_id')) }}
  <br />
  {{ Form::label ('model_id', 'Model',  array('multiple' => true)) }}
  <br />
    <div>
      {{ Form::select ('model_id', array('0' => ' -- Please select -- ') + $models, $efficiency->model_id , array('id' => 'model_id')) }}
    </div>

  {{ Form::label ('shift_id', 'Shift',  array('multiple' => true)) }}
  <br />
  {{ Form::select ('shift_id', array('0' => ' -- Please select -- ') + $shifts, $efficiency->shift_id, array('id' => 'shift_id')) }}
  <br />
  {{ Form::label ('CT') }}
  <br />
  {{ Form::text('ct', $efficiency->CT, array('id'=>'ct')) }}
  <br />
  {{ Form::label ('Production Plan') }}
  <br />
  {{ Form::text('production_plan', $efficiency->production_plan, array('id'=>'production_plan')) }}
  <br />
  {{ Form::label ('Production Real') }}
  <br />
  {{ Form::text('production_real', $efficiency->production_real, array('id'=>'production_real')) }}
  <br />

  {{ Form::label ('comment', 'Comment') }}

  <br />
  {{ Form::textarea('comment', $efficiency->comment, array('id'=>'comment')) }}
  <br />
  {{ Form::label ('Search Problem') }}
  <br />
  {{ Form::text('search_problem', '', array('id'=>'search_problem')) }}

  <div id="result-search-problem" class="result-search-problem" style='width:55%'>
    ...
  </div>
  <hr />
    <div id="result-problem-efficiency">
      @if($defects->count())
        @foreach($defects as $item)
          <span class="add-problem-effciency" style="width:50%;" id="{{$item->defect->id}}"> {{ $item->defect->name }} ({{$item->quantity}}) 
            <a class="remove-row-problem"> X </a></span>
            <script type="text/javascript">
              createJSONProblems("<?php echo $item->defect->id?>", "<?php echo $item->quantity?>")
            </script>
        @endforeach
      @endif
    </div>
  <hr />
  <button id="edit-efficiency">Edit Efficiency</button> {{ link_to('efficiency', 'Cancel') }}
  {{ Form::close() }}
</div>
</div>
@stop