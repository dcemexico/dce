<!DOCTYPE html>
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    {{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/buscar.js') }}
	{{ HTML::style('css/layout.css')}}
    <title> Bienvenido </title>
 </head>
 <body>
	<div class="container">
		<header>
			
			<div class="content-user">
				<div class="user-account">
					<span> Bienvenido <b>{{ $user->name }} </b></span>
					<span> {{ HTML::image('images/users/koala.jpg', 'Iamgen no encintrada', 
						array('id' => 'users','title' => 'User', 'width' => '60px', 'class'=>'user-photo')) }} </span>
					
			    	<span>{{ link_to('/auth/logout', 'Cerrar') }}</span>
				</div>
				
			</div>
			

			<div class="search">
				<input class="search-input" type="text" placeholder="Search..." />
			</div>
			<div class="contant-menu-logo">
				<div class="logo">DCM de México</div>
				<nav>
					<ul class="trans-nav">
						<li class="active-page"><a>Home</a></li>
						<li>Personal</li>
						<li>Products
							<ul>
								<li>Modelo</li>
								<li>moldes</li>
							</ul>
						</li>

						<li>Production</li>
						<li>Plan</li>
					</ul>
				</nav>
			</div>
			
			<div class="data-content">
				<section class="dashboard">
					<ul>
						<li>Home /</li>
						<li>Uses /</li>
						<li class="active-dashboard">Edit</li>
					</ul>
				</section>
				<section class="data-production">
					<div class="assembly-data">Assembly: 1005 pz</div>
					<div class="press-data">Press: 5660 pz</div>
				</section>
			</div>
			

		</header>

		<div class="body-container">

			<div class="container">
				@yield('content')
			</div>
			
			<!--<div class="container">
				@yield('content')
			</div>
			-->
		</div>
	</div>



</body>
</html>
