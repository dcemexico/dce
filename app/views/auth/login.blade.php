<!DOCTYPE html>
<html lang="es" class="boxshadow">
 <head>
  <meta charset="utf-8">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Login </title>
    {{ HTML::style('css/login.css') }}
    {{ HTML::script('js/modernizr.custom.44129.js') }}
 </head>
 <body>
  <div class="content">
    <div id="content-login" class="content-login">
      <div class="content-logo">
        <span class="logo"> {{ HTML::image('images/icos/ico-log-in.png', 'Imagen not found', 
            array('id' => 'users','title' => 'User', 'width' => '30px')) }} DCM de México </span>
      </div>
      

      <div class="content-login-form">
        <h1> Log In to Intranet System </h1>
        <hr />
      @if(isset($error))
           <p> <strong class="error-login"> {{ $error }} </strong> </p>
        @endif 
        {{ Form::open(array('url' => 'auth/login')) }} 
           {{ Form::text('nick', '', array('placeholder'=>'Username')) }}
           {{ Form::password('password', array('placeholder'=>'Password')) }}
           {{ Form::submit('Sign in') }} 
        {{ Form::close() }}
      </div>
    </div>
  </div>
 </body>
</html>