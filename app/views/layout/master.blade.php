<!DOCTYPE html>
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    {{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/buscar.js') }}
	{{ HTML::style('css/layout.css')}}
	{{ HTML::script('js/pace.js') }}
	{{ HTML::script('js/user-login.js') }}
	{{ HTML::style('css/themes/pace-theme-flash.css') }}
	
    <title> DCE de México S.A. de C.V.</title>
 </head>
 <body>
	<div class="container">
		<header>
			<div class="content-user">
				<div class="user-account">
					<!--<span> Bienvenido <b>{{ $value = Session::get('nameUSer') }} </b></span>-->
					
						@if($value = Session::get('name-photo'))
					          <img src="../../images/users/{{ $valueImg = Session::get('name-photo') }}" class="user-photo" />
					    @else
					          <img src="../../images/users/nophoto.png" class="user-photo" />
					    @endif
						 {{ link_to('/auth/logout', 'Logout') }}

						 <span class="option-user">
						 	
						 			{{-- HTML::image('images/icos/config.png', 'Imagen not found', 
						array('id' => 'users','title' => 'User', 'width' => '20px', 'class'=>'imageHader')) --}}

						 		{{-- HTML::image('images/icos/leave.png', 'Imagen not found', 
						array('id' => 'users','title' => 'User', 'width' => '30px', 'class'=>'imageHader')) --}}
						 	
						 	
						</span>
					
			    	<!---->
				</div>
			</div>
			<!--<div class="search">
				<input class="search-input" type="text" placeholder="Search..." />
			</div>-->
			<div class="contant-menu-logo">
				<div class="logo">DCM de México</div>
				<nav>
					<ul class="trans-nav">
						<!-- class="active-page-->
						<li>{{ link_to('/', 'Home') }}</li>
						<li>{{ link_to('/users', 'Users') }}</li>
						<li>Products
							<ul>
								<li>{{ link_to('/models', 'Models') }}</li>
								<li>{{ link_to('/molds', 'Molds') }}</li>
							</ul>
						</li>
						<li>Lines/Celds
							<ul>
								<li>{{ link_to('/lines', 'Lines') }}</li>
								<li>{{ link_to('/shifts', 'Shift') }}</li>
							</ul>
						</li>
						<li>Production
							<ul>
								<!--<li>MTBF</li>-->
								<li>{{ link_to('/efficiency', 'Efficiency')}}</li>
								<li>{{ link_to('/production', 'Production')}}</li>
								
								<li>{{ link_to('/defects', 'Defects')}}</li>
								<li>{{ link_to('/problemproduction', 'Problem type')}}</li>
								
							</ul>
						</li>
						<li>{{ link_to('/plan', 'Plan') }}</li>

						<!--<li>Tags</li>-->
					</ul>
				</nav>
			</div>
			
			<div class="data-content">
				<!--<section class="dashboard">
					<ul>
						<li>Home /</li>
						<li> /</li>
						
					</ul>
				</section>-->
				<!--<section class="data-production">
					<div class="assembly-data">Assembly: 1005 pz</div>
					<div class="press-data">Press: 5660 pz</div>
				</section>-->
			</div>
		</header>
		<div>
			<span class="message-any-thing"></span>
			<!--<div class="container">-->
				@yield('content')
			<!--</div>-->
		</div>
	</div>
</body>
</html>
