@extends('layout.master')
{{ HTML::style('css/shifts.css') }}
@section('content')

    @if(Session::has('notice'))
        <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
    @endif
    <p> 
        {{ HTML::image('images/icos/mold.png', ' Imagen not found' ,array('class'=>'imageIcos')) }}
        {{ link_to ('shifts/create', ' Create new shift ') }} 
    </p>

    @if($shifts->count())
        <div class="content-data" id="content-production-shifts">
        <div class="title-content-data">List of Shifts </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th> Color </th>
                        <th> Description </th>
                        <!--<th> </th>-->
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($shifts as $item)
                    <tr>
                        <td>
                        @if(!is_null($item->color_identify))
                            {{ $item->color_identify }} 
                             <span style="position:absolute;width:40px;background-color:{{ $item->color_identify }}"> &nbsp; </span> 
                        @else
                            NONE
                        @endif
                        </td>
                        <td> {{ $item->shi_description }} </td>
                        @if($autoriz)
                            <td> 
                                <a href="shifts/{{$item->id}}/edit">{{ HTML::image('images/icos/edit.png', 'Imagen not found', array('class'=>'imageIcos')) }}</a>
                                <a class="shift_delete">
                                    <input id="shift_id" type="hidden" value="{{$item->id}}" />
                                    <img src="images/icos/delete.png" class="imageIcos">
                                </a> 
                            </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
        <p>no se encontro datos</p>
    @endif

    @stop
