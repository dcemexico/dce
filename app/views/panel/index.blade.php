@extends('layout.master')
@section('content')

<script type="text/javascript" src="../js/post-user.js"></script>

<script type="text/javascript">
/*
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {


      //var data = array("{{$production}}");
        // Create the data table.
        var data = new google.visualization.DataTable(
            {
             cols: [{id: 'task', label: 'Task', type: 'string'},
                      {id: 'hours', label: 'Hours per Day', type: 'number'}],
             rows: [{c:[{v: 'Work'}, {v: 11}]},
                    {c:[{v: 'Eat'}, {v: 2}]},
                    {c:[{v: 'Commute'}, {v: 2}]},
                    {c:[{v: 'Watch TV'}, {v:2}]},
                    {c:[{v: 'Sleep'}, {v:7, f:'7.000'}]}
                   ]
           }
          );
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');

        

        // Set chart options
        var options = {'title':'Production',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }*/
    </script>

   @if(Session::has('notice'))
    <p><strong class="message-successful">  {{  Session::get('notice') }} {{ HTML::image('images/icos/message.png', 'Imagen not found', array('class'=>'imageIcos')) }}</strong></p>
  @endif
	
	
	<div class="content-data" id="content-production-charts">
		<!--<div id="chart_div"></div>-->
    <div class="title-content-data">Production</div>
    <table>
        <thead>
          <tr>
            <th>Linea</th>
            <th>Modelo</th>
            <th>Mold</th>
            <th>Quantity</th>
            <th>With Plan</th>
          </tr>
        </thead>
        <tbody>
          @if($production->count())
              @foreach($production as $item)
              <tr>

                  @if(!is_null($item->line_id))
                  <td> {{ $item->line->lin_description}} </td>
                  @else
                  <td></td>
                  @endif

                  @if($item->model_id > 0)
                    <td> {{ $item->model->codigo }} </td>
                  @else
                    <td></td>
                  @endif

                  @if($item->model_id > 0)
                    <td> {{ $item->model->mold->codigo }} </td>
                  @else
                    <td></td>
                  @endif

                  <td> {{ $item->quantity }} </td>

                  @if(!is_null($item->plan_id))
                    <td> YES </td>
                  @else
                    <td>NO</td>
                  @endif
              </tr>
              @endforeach
          @else
            <tr><td colspan="6">Not found data</td></tr>
          @endif
        </tbody>
      </table>
	</div>
  <div class="content-data" id="content-production-production-defects">
    <div class="title-content-data">Production Defects </div>
    <div>
      <table>
        <thead>
          <tr>
            <th>Linea</th>
            <th>Defecto</th>
            <th>Modelo</th>
            <th>Fecha</th>
          </tr>
        </thead>
        <tbody>
          @if($def_production->count())
              @foreach($def_production as $item)
              <tr>
                  <td> {{-- $item->line->lin_description --}} </td>
                  <td> {{ $item->defect->name }} </td>
                  <td> {{-- $item->model->codigo --}} </td>
                  <td> {{ $item->created_at }} </td>
              </tr>
              @endforeach
          @else
            <tr><td colspan="6">Not found data</td></tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>
  <div class="content-data" id="content-post-user">
    <div class="title-content-data">  <span>{{-- HTML::image('images/icos/post.png', 'Imagen not found', 
            array('id' => 'users','title' => 'User', 'width' => '20px', 'class'=>'imageHader')) --}}</span> Posts</div>
    <div class="content-post" id="content-post">
      @if($posts->count())
        @foreach($posts as $item)
          <div class="row-post" id="row-post">
          <span class="user-post">
            @if($item->user->photographic)
                    <img src="../../images/users/{{ $item->user->photographic }}" class="user-photo" />
              @else
                    <img src="../../images/users/nophoto.png" class="user-photo" />
              @endif
            <span class="post-name-user">{{ $item->user->name}}</span>
          </span>
          <?php
            $date_today = date('Y-m-d');
            $time_now = date('H:i:s');
            $time_real = '';
            $date_d =  explode(' ', $item->created_at);
            if($date_d[0] == $date_today){

              $time_d = explode(':', $date_d[1]);
              $time_t = explode(':', $time_now);
              if ($time_d[0] != $time_t[0]) {
                  $time_real .= str_replace('-','', $time_t[0] - $time_d[0].'hrs, ');
              }if ($time_d[1] != $time_t[1]) {
                  $time_real .= str_replace('-','',$time_d[1] - $time_t[1].'min, ');
              }if ($time_d[2] != $time_t[2]) {
                  $time_real .= str_replace('-','',$time_d[2] - $time_t[2].'seg');  
              }
              $time_real .= ' ago';
            }else{
              $time_real= $item->created_at;
            }
          ?>
          <span class="date-post"> {{ $time_real }}</span>
          <div class="content-post-user">{{ $item->body }}</div>
        </div>
        @endforeach
      @else
        Not found data
      @endif
    </div>
    <div class="content-new-post">
      <textarea class="new-post-content" id="new-post-content"></textarea>
      <button class="btn-raplay-post" id="btn-replay-post">Reply</button>
    </div>
  </div>
  <div class="content-data" id="content-production-plan">
    <div class="header-content">
      <div class="title-content-data">Production Plan </div>
      <!--<div class="date">{{-- HTML::image('images/icos/date.png', 'Imagen not found', 
            array('id' => 'users','title' => 'User', 'width' => '20px', 'class'=>'imageHader')) --}}</div>-->
    </div>
    
    <div>
      <table class="table-content-production-plan">
        <thead>
          <tr>
            <th>Code</th>
            <th>Line</th>
            <th>Shift</th>
            <th>Date</th>
            <th>Quantity</th>
            <th>Quantity CC</th>
            <th>Quantity GR</th>
            <th>Orden</th>
          </tr>
        </thead>
        <tbody style="height:90px;overflow: scroll;">
           @if($plan->count())
                @foreach($plan as $item)
                <tr>

                  @if($item->model_id > 0)
                  <td> {{$item->model->mod_description }} </td>
                  @else
                  <td></td>
                  @endif

                  @if(!is_null($item->line_id))
                  <td> {{$item->line->lin_description }} </td>
                  @else
                  <td></td>
                  @endif
                    <td> {{ $item->shift->shi_description }} </td>
                    <td> {{ $item->production_date }} </td>
                    <td> {{ $item->quantity }} </td>

                  @if($item->sync_quantity_cc > 0)
                  <td> {{$item->sync_quantity_cc }} </td>
                  @else
                  <td></td>
                  @endif

                  @if($item->sync_quantity_gr > 0)
                  <td> {{$item->sync_quantity_gr }} </td>
                  @else
                  <td></td>
                  @endif

                  <td><img src="../images/icos/{{$item->status_plan}}.png" class="imageIcosTable" /></td>
                </tr>
                @endforeach
            @else
              <tr><td colspan="6">Not found data</td></tr>
            @endif
        </tbody>
      </table>
    </div>
  </div>
  <?php
    $fechas = array();
    $fechas_visualiza = array();
    $fechas_visualiza_dia = array();

    for ($i=0; $i < 8 ; $i++) { 
         $n_fecha = strtotime('+'.$i.' day', strtotime($date_today));
         $fechas[$i] = date('Y-m-d', $n_fecha);
         $fechas_visualiza[$i] = date('d M', $n_fecha);
         $fechas_visualiza_dia[$i] = date('D', $n_fecha);
    }
  ?>
  <div>
    <table>
      <thead>
        <tr>
          <td colspan="">
            @if($shifts->count())
              @foreach($shifts as $i_shift)
                <span style="background:{{$i_shift->color_identify}};color:#ccc;padding:5px;">{{$i_shift->shi_description}}</span>
              @endforeach
            @else
              NOT DATA
            @endif
          </td>
          <td colspan="9"></td>
        </tr>
        <tr>
          <th rowspan="2">Description</th>
          <?php for ($i=0; $i < sizeof($fechas_visualiza); $i++) { ?>
            <th style="background:#ccc;"><?php echo $fechas_visualiza[$i] ?></th>
          <?php } ?>
        </tr>
        <tr>
          <?php for ($i=0; $i < sizeof($fechas_visualiza_dia); $i++) { ?>
            <th><?php echo $fechas_visualiza_dia[$i] ?></th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
          @if($lines->count())
            @foreach($lines as $i_line)
            <tr>
            <?php
              $plan = Plan::with('line', 'user', 'model.mold', 'shift')->where('line_id', $i_line->id)->where('production_date','>=', $date_today)->where('production_date', '<=', $fechas_visualiza_dia[7])->get();
            ?>
              @if($plan->count())
              <td colspan="11" style="font-weight: bolder">{{$i_line->lin_description}}</td>
                @foreach($plan as $i_plan)

                <tr>
                  @if($i_plan->model_id > 0)
                      <td>{{$i_plan->model->codigo}} / 
                      @if(!is_null($i_plan->model->mold))
                          {{ $i_plan->model->mold->codigo }} </td>
                      @else
                          </td>
                      @endif
                  @else
                      <td></td>
                  @endif

                  <?php
                    for ($i=0; $i < sizeof($fechas); $i++) {
                      if ($fechas[$i] === $i_plan->production_date) {
                          $style = 'background:'.$i_plan->shift->color_identify.';color:#fff;';
                          if ($i_plan->status_plan === 2) {
                            $style .= "border-bottom:3px solid red;";
                          }
                        echo '<td style="'.$style.'">'.$i_plan->quantity.'</td>';
                      }else{
                        echo '<td></td>';
                      }  
                    }
                  ?>
                  <!--<td>-->
                    <?php
                      //$discussion = Discussion::where('plan_id', $i_plan->id)->select('*' ,DB::raw('count(type) as count'))->groupBy('type')->get();
                    ?>
                      {{-- @if($discussion->count())
                        @foreach($discussion as $i_discussion)
                          @if($i_discussion->type == 'l')
                            {{ $i_discussion->count() }}
                            {{ HTML::image('images/icos/l.png', 'Imagen',array('id' => 'users','title' => 'Like', 'width' => '20px')) }}
                          @elseif($i_discussion->type == 'u')
                            {{ $i_discussion->count() }}
                            {{ HTML::image('images/icos/u.png', 'Imagen',array('id' => 'users','title' => 'Like', 'width' => '20px')) }}
                          @endif
                        @endforeach
                      @else

                      @endif --}}
                   
                  <!--</td>-->
                </tr>
                @endforeach
              @endif
            </tr>
            @endforeach
          @else
          @endif
      </tbody>
    </table>
  </div>
@stop