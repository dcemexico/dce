<?php
	class Mold extends Eloquent{
		/*public function production(){
			return $this->hasMany('Production', 'model_id');
		}*/
		//public function lines() {
			//return $this->belongsToMany('Line', 'productions', 'model_id', 'line_id');
		//}

		public function models() {
			return $this->hasMany('Model');
		}

		protected $table = 'molds';

		public static $rules = array(
	      'codigo' => 'required|min:2',
	      'mol_description' => 'required'
	   );

		public static $messages = array( /////////////////////////Edit/////////////////////////////////
	      	'codigo.required' => 'Codigo obligatorio.',
	     	'mol_description.required' => 'Requerido'
	   	);
		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      //$reglas['email'] = str_replace('id', $id, self::$rules['email']);
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }
	}
?>