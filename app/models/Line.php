<?php
	class Line extends Eloquent {
		/*public function production(){
			return $this->belongsTo('Production');
		}*/
		public function modelos() {
			//return $this->belongsToMany('Model', 'productions', 'line_id', 'model_id');
		}

		public function plans() {
			return $this->hasMany('Plan');
		}
		protected $table = 'lines';

		public static $rules = array(
	      'lin_description' => 'required|min:2'
	   );

		public static $messages = array( /////////////////////////Edit/////////////////////////////////
	      	'lin_description.required' => 'Requerido.'
	   	);
		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }

	}
?>