<?php
	class Production extends Eloquent {

		public function line() {
			return $this->belongsTo('Line');
		}
		public function user() {
			return $this->belongsTo('User');
		}
		public function model() {
			return $this->belongsTo('Model');
		}
		public function arduino() {
			return $this->belongsTo('Arduino');
		}

		protected $table = 'productions';
	}
?>