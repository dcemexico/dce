<?php
	class Model extends Eloquent{
		/*public function production(){
			return $this->hasMany('Production', 'model_id');
		}*/
		public function mold(){
			return $this->belongsTo('Mold');//->select(array('mol_description')); //, 'id as id_mold'
		}

		protected $table = 'models';

		public static $rules = array(
	      'codigo' => 'required|min:2',
	      'mod_description' => 'required',
	      'mold_id' => 'required',
	   );

		public static $messages = array( /////////////////////////Edit/////////////////////////////////
	      	'codigo.required' => 'Codigo obligatorio.',
	      	'mod_description.min' => 'Modelo requerido.',
	     	'mold_id.numeric' => 'Mold requerido'
	   	);
		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      //$reglas['email'] = str_replace('id', $id, self::$rules['email']);
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }
	}
?>