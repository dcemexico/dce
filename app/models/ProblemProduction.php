<?php
	class ProblemProduction extends Eloquent {
		
		public static $rules = array(
			'name' => 'required',
	      'pp_description' => 'required|min:2',
	   );

		public static $messages = array( /////////////////////////Edit/////////////////////////////////
			'name' => 'Requerido',
	      	'pp_description.required' => 'Requerido'
	   	);

		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }

		protected $table = 'problem_production';

	}
?>