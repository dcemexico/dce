<?php
	class Plan extends Eloquent {
		/*public function production(){
			return $this->belongsTo('Production');
		}*/
		public function line() {
			return $this->belongsTo('Line');
		}
		public function user() {
			return $this->belongsTo('User');
		}
		public function model() {
			return $this->belongsTo('Model');
		}
		
		public function shift() {
			return $this->belongsTo('Shift');
		}

		protected $table = 'plan';
	}
?>