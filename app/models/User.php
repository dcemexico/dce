<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
		public function posts() {
			return $this->hasMany('Post');
		}

		public function plans() {
			return $this->hasMany('Plan');
		}

		public function production (){
			//return $this->hasMany('Production', 'user_id');
		}

		public function usertype (){
			return $this->belongsTo('usertype');
		}

		public static $rules = array(
	      'name' => 'required|min:2',
	      'email' => 'required|email|unique:users,email,id',
	      'password' => 'required',
	   );

		public static $messages = array(
	      	'name.required' => 'El nombre es obligatorio.',
	      	'name.min' => 'El nombre debe contener al menos dos caracteres.',
	      	'email.required' => 'El email es obligatorio.',
	      	'email.email' => 'El email debe contener un formato válido.',
	      	'email.unique' => 'El email pertenece a otro usuario.',
	      	'password.required' => 'La contraseña es obligatoria.',
	      	'level.required' => 'El nivel es obligatorio.',
	     	'level.numeric' => 'El nivel debe ser numérico.'
	   	);
		public static function validate($data, $id=null){
	      $reglas = self::$rules;
	      $reglas['email'] = str_replace('id', $id, self::$rules['email']);
	      $messages = self::$messages;
	      return Validator::make($data, $reglas, $messages);
	   }


	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

}