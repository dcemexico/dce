<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('shifts', function ($table) {
			$table->create();
			$table->increments('id');
			$table->string('shi_description');
			$table->string('color_identify');
			$table->time('start_time');
			$table->time('finish_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('shifts');
	}

}
