<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('plan', function ($table) {
			$table->create();
			$table->increments('id');
			$table->integer('model_id');
			//$table->integer('mold_id');
			$table->integer('user_id');
			$table->integer('line_id');
			$table->integer('shift_id');
			$table->datetime('production_date');
			//$table->increments('order');//Se elimino
			$table->text('comment');
			$table->text('order');
			$table->integer('quantity');
			$table->integer('status_data');
			$table->integer('status_plan');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('plan');
	}

}
