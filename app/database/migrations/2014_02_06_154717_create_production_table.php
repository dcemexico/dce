<?php

use Illuminate\Database\Migrations\Migration;

class CreateProductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('productions', function ($table) {
			$table->create();
			$table->increments('id');
			$table->integer('id_user');//Change id_componente Example 'user_id'
			$table->integer('line_id');//Change
			$table->integer('model_id');//Change
			$table->integer('arduino_id');//Change
			$table->integer('plan_id');//Change
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('productions');
	}

}