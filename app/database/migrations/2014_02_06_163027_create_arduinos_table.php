<?php

use Illuminate\Database\Migrations\Migration;

class CreateArduinosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('arduinos', function ($table) {
			$table->create();
			$table->increments('id');
			$table->string('ard_description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('arduinos');
	}

}