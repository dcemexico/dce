<?php

use Illuminate\Database\Migrations\Migration;

class CreateMoldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('molds', function ($table) {
			$table->create();
			$table->increments('id');
			$table->string('codigo');
			$table->string('mol_description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('molds');
	}

}