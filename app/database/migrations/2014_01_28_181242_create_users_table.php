<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function ($table){
			$table->create();
			$table->increments('id');
			$table->string('email');
			$table->string('nick');
	       	$table->string('name');
	       	$table->string('password');
	       	$table->integer('usertype_id');
	       	$table->boolean('active');
	       	$table->string('photographic');
	       	$table->timestamps();
		});
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		//
	}

}