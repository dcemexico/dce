var da = new Date();
	var d = da.getDate();
	var m =('0'+(da.getMonth()+1)).slice(-2);
	var y = da.getFullYear();

	var date_plan_sel = y+'-'+m+'-'+d;

	$(function () {
		$("#input_date").datepicker({
			dateFormat: 'yy-mm-dd',
			//minDate: 0,
			maxDate: "+15D",
			onSelect: function(dateText, inst) { 
				date_plan_sel = dateText;
		    }
		});
	});