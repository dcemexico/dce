var jsonObj = [];
var item = {};

function createJSONProblems(_id, _qua) {
	jsonObj.push( { id: _id, qua:_qua} );
	console.log(jsonObj);
}



$(function () {
	$('#edit-efficiency').on('click', function () {
		if ($("#line_id").length > 0 && 
			$("#efficiency_id").length > 0 && 
			$("#comment").length > 0 && 
			$("#shift_id").length > 0 && 
			$("#model_id").length > 0 &&
			$("#ct").length > 0 &&
			$("#production_plan").length > 0 &&
			$("#production_real").length > 0 &&   
			$.isNumeric($("#production_plan").val()) &&
			$.isNumeric($("#production_real").val()) &&
			$.isNumeric($("#ct").val())) {

			$.ajax({
				url: "/efficiency/store",
				timeout: 10000,
				type: 'GET',
				dataType: 'JSON',
				data: {id_eff: $("#efficiency_id").val(),
						line: $("#line_id").val(),
						model: $("#model_id").val(),
						shift: $("#shift_id").val(),
						ct: $("#ct").val(),
						pp:$("#production_plan").val(),
						pr:$("#production_real").val(),
						comment:$("#comment").val(),},
				beforeSend: function () {

				},error: function () {
				}, success: function (res) {
					alert(res);
					/*
					var jsond = JSON.parse(JSON.stringify(jsonObj));

					$.each(jsond, function (i, v) {
						
						insert_p(res, v.id, v.qua);
					});*/
					
				}
			});
		}else{
			alert("Aun no!");
		}

	});

	function insert_p (id, id_pro, qua){
		//$.post('efficiency/insertp/'+id+'/'+id_pro+'/'+qua, function (d) {
			$.ajax({
				datatype: 'html',
				url: "/efficiency/insertp/"+id+'/'+id_pro+'/'+qua,
				timeout: 10000,
				beforeSend: function () {
				},error: function () {
					alert("Error insert!");
				}, success: function (res) {
					jsonObj = [];
					$("#search_problem").val('');
					$('#result-search-problem').html('...');
					$('#result-problem-efficiency').html('');
				}
			});
		//});
	}

	$("#mold_id").on('change' ,function () {
		var json_model = function (id){
			$.ajax({
			url: "/plan/model/"+id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
			}, success: function (res) {
				$("#model_id").html("<option> -- Please select -- </option>");
					$.each(res, function (i, d) {
						$("#model_id").append("<option value='"+d.id+"'>"+d.codigo+"</option>");
					});
				}
			});
		};

		json_model(this.value);

	});


	var t = true;
	$("#search_problem").on('keyup', function (e) {

		var key = $("#search_problem").val();
		var div= '';
		var jsond = JSON.parse(JSON.stringify(jsonObj));
		

		if(key.length > 0){
			$.get("/efficiency/problems/"+key, function(data){

				if (data.length > 0) {
					$.each(data, function (i, obj) {
						$.each(jsond, function (i, v) {
							if(v.id != obj.id) {
								t = true;
								return true;
							}else{
								t = false;
								return false;
							}
						});

						if (t) {
							div += '<p class="row-problem-modal" id="'+obj.id+'"><label lcass>'+obj.name+'</label> <input class="quantity-problem" style="width:60px" type="text" placeholder="Quantity"></input> <button class="btn-add-row-problem">Add</button><p>';
						}
						
					});
					
					$("#result-search-problem").html(div);
				}else{
					$("#result-search-problem").html('...');
				}


			});
		}else{
			$("#result-search-problem").html('...');
		}
	});

	$('#result-search-problem').on('click','.btn-add-row-problem', function (e) {
		e.preventDefault();
		var row = $(this).parent();
		var d = '';
		var id = row.attr('id');
		var description_problem = $(row).children('label').text();
		var quantity =  $(row).children('input').val();
		if (quantity > 0) {
			if ($.isNumeric(quantity)) {
				d = '<span id="'+id+'" class="add-problem-effciency" style="width:50%;">'+description_problem+' ('+quantity+') <a class="remove-row-problem"> X </a></span>';
				createJSONProblems(id,quantity);
			}else{
			}
			$(row).remove();
			$("#result-problem-efficiency").append(d);
		}else{
			alert("Quantity!");
		}
	});

$("#result-problem-efficiency").on('click', '.remove-row-problem',function (e) {
		var parent = jQuery(this).parent();
		var id = parent.attr('id');
		//alert(id);
		$("#"+id).remove();
		var jsond = JSON.parse(JSON.stringify(jsonObj));
	 	$.each(jsond, function (i, r) {
	 		if (r['id'] == id) {
	 			jsonObj.splice(i, 1);
	 			console.log(jsonObj);
	 			$(this, ".add-problem-effciency").remove();
	 		}
	 	});
	});
});


