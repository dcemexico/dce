$(function () {
	$(".user_delete").on('click', function (e) {
		e.preventDefault();
		var id = $('input', this).val();
		$.ajax({
			url: "/users/delete/"+id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
				alert('Error!')
			}, success: function (res) {
				if (res == 0) {
					$('.user-data-'+id).remove();
				} 
				else if (res == 2){
					alert("you can't your self acount!");
				}else{
					alert('Error!');
				}
			}
		});
	});
});