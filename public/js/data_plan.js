var da = new Date();
var d = da.getDate();
var m =('0'+(da.getMonth()+1)).slice(-2);
var y = da.getFullYear();



var date_plan_sel = y+'-'+m+'-'+d;
//alert(date_plan_sel);
$(function () {
	$("#plans").datepicker({
		dateFormat: 'yy-mm-dd',
		minDate: 0,
		maxDate: "+15D",
		onSelect: function(dateText, inst) { 
			date_plan_sel = dateText;
			data_plan_select(dateText);
	    }
	});
	

	$("#add_plan").on('click', function () {
		$(".modal").slideDown("slow", function () {
			$.get("plan/form", function (data) {
				$(".modal").html(data);
				//$("body > *").not(".modal").addClass('modal-opacity');
			})
		});
	});

	$(".modal").on('click', '.close-modal', function () {
		$(".modal").slideUp("slow", function () {
				//$("body > *").not(".modal").removeClass('modal-opacity');
				$(".modal").html("");
		});
	});

	

	$(".modal").on('change', '#mold_id' ,function () {
		var json_model = function (id){
			$.ajax({
			url: "/plan/model/"+id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
			}, success: function (res) {
				$("#model_id").html("<option> -- Please select -- </option>");
					$.each(res, function (i, d) {
						$("#model_id").append("<option value='"+d.id+"'>"+d.codigo+"</option>");
					});
				}
			});
		};

		json_model(this.value);

	});

	$(".modal").delegate("#date_plan", "focusin", function () {
		$(this).datepicker({
			dateFormat: 'yy-mm-dd',
			minDate: 0,
        	maxDate: "+15D"
		});
	});


	$(".modal").on('click', '#insert-plan', function () {
		if ($("#line_id").length && 
			$("#date_plan").length && 
			$("#comment").length && 
			$("#model_id").length && 
			$("#shift_id").length && 
			$("#quantity").length &&
			$("#model_id").val().length > 0 &&
			$.isNumeric($("#quantity").val())) {

			$.ajax({
				url: "/planInsert/"+$("#model_id").val()+"/"+$("#line_id").val()+"/"+$("#shift_id").val()+"/"+$("#comment").val()+"/"+$("#quantity").val()+"/"+$("#date_plan").val(),
				timeout: 10000,
				beforeSend: function () {
				},error: function () {
				}, success: function (res) {
					$("#line_id").val('0');
					$("#mold_id").val('0');
					$("#model_id").find('option').remove().end().append('<option value="0"> -- Please select -- </option>');
					$("#shift_id").val('0');
					$("#date_plan").val('');
					$("#comment").val('');
					$("#quantity").val('');
					alert("Correct");
				}
			});
		}else{
			alert("Aun no!");
		}

	});


});


/*
setInterval(function() {
		$.ajax({
	url: "/plan/data/"+date_plan_sel,
	dataType: "JSON",
	timeout: 10000,
	beforeSend: function () {
		//$("#data_production_line").html("buscando");

	},error: function () {
		$(".message-any-thing").html("Error!");
	}, success: function (res) {
		if (res.length > 0) {

			var div = "";
			var id_plan;
			//$(".message-any-thing").slideDown(1000);
			//$(".message-any-thing").html("Actualizando");
			$.each(res, function (r, obj){
				div += "<tr>";
                        div += "<td> "+obj.model.mod_description+" </td>";
                        div += "<td> "+obj.model.mold.mol_description+" </td>";
                        div += "<td> "+obj.shift.shi_description+" </td>";
                        div += "<td> "+obj.production_date+" </td>";
                        div += "<td> "+obj.comment+" </td>";
                        div += "<td> "+obj.quantity+" </td>";
                        div += "<td> "+obj.created_at+" </td>";
                        div += "<td> "+obj.updated_at+" </td>";
                        div += "<td> <img src='images/icos/open.png' class='imageIcos' /></td>";
                    div += "</tr>";
                    id_plan = obj.id;
			});
			$("#data_plan").append(div);
			change_status_plan(id_plan);
			//$(".message-any-thing").slideUp("slow");
			}
		}
	});

}, 4000);*/

$(function () {
	$("#data_plan").on('click', '.edit-plan', function () {
		var id = $('input', this, '#data_plan').val();
		window.location.href= "plan/edit/"+id;
	});	
});

function data_plan_select(date) {
	event.preventDefault();

	$.getJSON("/plan/dates/"+date, function (res){
		var div = "";
		$.each(res, function(i, obj){
		    div += "<tr>";
		    	if (obj.line.id) {
		    		div += "<td> "+obj.line.lin_description+" </td>";
		    	}else{
		    		div += "<td></td>";
		    	}

		    	if (obj.model_id) {
		    		div += "<td> "+obj.model.codigo+" </td>";
		    	}else{
		    		div += "<td></td>";
		    	}
	            div += "<td> "+obj.shift.shi_description+" </td>";
	            div += "<td> "+obj.order+" </td>";
	            div += "<td> "+obj.production_date+" </td>";
	            if (obj.comment) {
		    		div += "<td> "+obj.comment+" </td>";
		    	}else{
		    		div += "<td></td>";
		    	}
	            div += "<td> "+obj.quantity+" </td>";

	            div += "<td> <img src='images/icos/"+obj.status_plan+".png' class='imageIcos' /></td>";
	            div += "<td> <a class='edit-plan'>Edit <input type='hidden' value='"+obj.id+"' /> </a></td>"
	        div += "</tr>";
        	id_plan = obj.id;
	    });

	    $("#data_plan").html(div);
		/*var a = JSON.parse(res);
				$.each(a, function (r, obj){
					
				});	
				*/
	});
	/*
	$.ajax({
		url: "/plan/dates/"+date,
		dataType: "html",
		timeout: 10000,
		beforeSend: function () {
		},error: function () {
			$(".message-any-thing").html("Error!");
		}, success: function (res) {
			if (res.length > 0) {
				var div = "";
				var id_plan;
				var a = JSON.parse(res);
				$.each(a, function (r, obj){
					div += "<tr>";
					 		div += "<td> "+obj.line.lin_description+" </td>";
	                        div += "<td> "+obj.model.codigo+" </td>";
	                        div += "<td> "+obj.model.mold.codigo+" </td>";
	                        div += "<td> "+obj.shift.shi_description+" </td>";
	                        div += "<td> "+obj.production_date+" </td>";
	                        div += "<td> "+obj.comment+" </td>";
	                        div += "<td> "+obj.quantity+" </td>";
	                        div += "<td> "+obj.created_at+" </td>";
	                        div += "<td> "+obj.updated_at+" </td>";
	                        div += "<td> <img src='images/icos/"+obj.status_plan+".png' class='imageIcos' /></td>";
	                        div += "<td> <a src='../plan/edit/"+obj.id+"'>Edit </a></td>"
	                    div += "</tr>";
	                    id_plan = obj.id;
				});	
				//alert(a.updated_at);
				//$.each(res, function (r, obj){
					/*
					//alert(r[obj]);
					*/
				//});
				/*$("#data_plan").html(""+div+"");
			}else{
				$("#data_plan").html("");
			}
		}
	});*/
}


function change_status_plan(id){
	$.ajax({
		url: "/plan/statuschange/"+id,
		timeout: 10000,
		beforeSend: function () {
		},error: function () {
			alert("Error!");
		}, success: function (res) {
		}
	});
}

