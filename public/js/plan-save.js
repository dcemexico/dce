$(function (){
	$("#mold_id").on('change', function () {
		var json_model = function (id){
			$.ajax({
			url: "/plan/model/"+id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
			}, success: function (res) {
				$("#model_id").html("<option> -- Please select -- </option>");
					$.each(res, function (i, d) {
						$("#model_id").append("<option value='"+d.id+"'>"+d.codigo+"</option>");
					});
				}
			});
		};
		json_model(this.value);
	});
});

$(function () {
	$("#date_plan").datepicker({
		dateFormat: 'yy-mm-dd',
		minDate: 0,
		maxDate: "+15D"
	});
});


$(function () {
	$("#edit-plan").on('click', function () {
		if ($("#line_id").length && 
			$("#date_plan").length && 
			$("#comment").length && 
			$("#model_id").length && 
			$("#shift_id").length && 
			$("#quantity").length &&
			$("#plan_id").length &&
			$("#model_id").val().length > 0 &&
			$.isNumeric($("#quantity").val())) {
			//alert($('input[name=status_plan]:checked').val());
			$.ajax({
				url: "/updatePlan/"+$("#plan_id").val()+"/"+$("#model_id").val()+"/"+$("#line_id").val()+"/"+$("#shift_id").val()+"/"+$("#date_plan").val()+"/"+$("#comment").val()+"/"+$("#quantity").val()+"/"+$('input[name=status_plan]:checked').val(),
				timeout: 10000,
				beforeSend: function () {

				},error: function () {
				}, success: function (res) {
					window.location ="/plan"
				}
			});
		}else{
			alert("Error form!");
		}

	});
});

