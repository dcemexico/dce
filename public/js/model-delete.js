$(function () {
	$(".model_delete").on("click", function (e) {
		e.preventDefault();
		var id = $('input', this).val();
		$.ajax({
			url: "/models/delete/"+id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
				alert('Error!')
			}, success: function (res) {
				if (res == 0) {
					$('.model-data-'+id).remove();
				}else{
					alert('Error!');
				}
			}
		});
	});
});