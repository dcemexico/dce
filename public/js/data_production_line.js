//data_production_line();
//datepickerproduction();
function data_production_line() {

	setInterval(function() {
  		$.ajax({
		url: "/production/line",
		dataType: "JSON",
		timeout: 10000,
		beforeSend: function () {
			//$("#data_production_line").html("buscando");
		},error: function () {
			$("#data_production_line").html("error");
		}, success: function (res) {
			var div = "";
			$.each(res, function (r, obj){
				div += "<tr>";
                        div += "<td> "+obj.line.lin_description+" </td>";
                        div += " <td> "+obj.quantity+" </td>";
                        div += " <td> "+obj.model.mod_description+" </td>";
                        div += "<td> "+obj.arduino.ard_description+" </td>";
                    div += "</tr>";
			});

			$("#data_production_line").html(div);
			}
		});

	}, 2000);
}

//function datepickerproduction() {
	var da = new Date();
	var d = da.getDate();
	var m =('0'+(da.getMonth()+1)).slice(-2);
	var y = da.getFullYear();

	var date_plan_sel = y+'-'+m+'-'+d;

	$(function () {
		$("#production_datepicker").datepicker({
			dateFormat: 'yy-mm-dd',
			//minDate: 0,
			maxDate: "+15D",
			onSelect: function(dateText, inst) { 
				date_plan_sel = dateText;
				data_production_select(dateText);
		    }
		});
	});
//}
function data_production_select(date) {
	$.ajax({
		url: "/production/line/"+date,
		dataType: "JSON",
		timeout: 10000,
		beforeSend: function () {
		},error: function () {
			$(".message-any-thing").html("Error!");
		}, success: function (res) {
			if (res.length > 0) {
				var div = "";
				//var id_plan;
				$.each(res, function (r, obj){
					div += "<tr>";
	                        div += "<td> "+obj.line.lin_description+" </td>";
	                        div += "<td> "+obj.quantity+" </td>";
	                        if (obj.model) {
	                        	div += "<td> "+obj.model.mod_description+" </td>";
	                        }else{
	                        	div += "<td> NO DATA</td>";
	                        }
	                        
	                        div += "<td> "+obj.arduino.ard_description+" </td>";

	                        if (obj.plan_id) {
	                        	div += "<td> YES </td>";
	                        }else{
	                        	div += "<td>NO</td>";
	                        }
	                    div += "</tr>";
	                    //id_plan = obj.id;
				});
				$("#data_production_line").html(div);
			}else{
				$("#data_production_line").html("");
			}
		}
	});
}