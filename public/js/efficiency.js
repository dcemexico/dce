var jsonObj = [];
var item = {};
function createJSONProblems(_id, _qua) {
	jsonObj.push( { id: _id, qua:_qua} );
	console.log(jsonObj);
}


$(function () {

	$("table tr td").on('click', function () {

		$(this).addClass('select_row');
	});

	$("table tr td").on('dblclick', function () {

		$(this).removeClass('select_row');
	});
	//--------
	$(".modal").on('click', '#insert-efficiency', function () {
		if ($("#line_id").length > 0 && 
			$("#comment").length > 0 && 
			$("#shift_id").length > 0 && 
			$("#model_id").length > 0 &&
			$("#ct").length > 0 &&
			$("#production_plan").length > 0 &&
			$("#production_real").length > 0 &&   
			$.isNumeric($("#production_plan").val()) &&
			$.isNumeric($("#production_real").val()) &&
			$.isNumeric($("#ct").val())) {

			$.ajax({
				url: "/efficiencyInsert/"+$("#line_id").val()+"/"+$("#model_id").val()+"/"+$("#shift_id").val()+"/"+$("#ct").val()+"/"+$("#production_plan").val()+"/"+$("#production_real").val()+"/"+$("#comment").val(),
				timeout: 10000,
				beforeSend: function () {
				},error: function () {
				}, success: function (res) {
					$("#line_id").val('0');
					$("#mold_id").val('0');
					$("#model_id").find('option').remove().end().append('<option value="0"> -- Please select -- </option>');
					$("#shift_id").val('0');
					$("#ct").val('');
					$("#production_real").val('');
					$("#production_plan").val('');
					$("#comment").val('');
					alert("Correct");
					var jsond = JSON.parse(JSON.stringify(jsonObj));

					$.each(jsond, function (i, v) {
						
						insert_p(res, v.id, v.qua);
					});
					
				}
			});
		}else{
			alert("Aun no!");
		}

	});

function insert_p (id, id_pro, qua){
	//$.post('efficiency/insertp/'+id+'/'+id_pro+'/'+qua, function (d) {
		$.ajax({
			datatype: 'html',
			url: "/efficiency/insertp/"+id+'/'+id_pro+'/'+qua,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
				alert("Error insert!");
			}, success: function (res) {
				jsonObj = [];
				$("#search_problem").val('');
				$('#result-search-problem').html('...');
				$('#result-problem-efficiency').html('');
			}
		});
	//});
}
	/////////----------

	$("#add_efficiency").on('click', function () {
		$(".modal").slideDown("slow", function () {
			$.get("/efficiency/form", function (data) {
				$(".modal").html(data);
				//$("body > *").not(".modal").addClass('modal-opacity');
			});
		});
	});

	$(".modal").on('click', '.close-modal', function () {
		jsonObj = [];
		$(".modal").slideUp("slow", function () {
				//$("body > *").not(".modal").removeClass('modal-opacity');
				$(".modal").html("");

		});
	});

	$(".modal").on('change', '#mold_id' ,function () {
		var json_model = function (id){
			$.ajax({
			url: "/plan/model/"+id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
			}, success: function (res) {
				$("#model_id").html("<option> -- Please select -- </option>");
					$.each(res, function (i, d) {
						$("#model_id").append("<option value='"+d.id+"'>"+d.codigo+"</option>");
					});
				}
			});
		};

		json_model(this.value);

	});

	var t = true;
	$(".modal").on('keyup', '#search_problem', function (e) {

		var key = $("#search_problem").val();
		var div= '';
		var jsond = JSON.parse(JSON.stringify(jsonObj));
		

		if(key.length > 0){
			$.get("/efficiency/problems/"+key, function(data){

				if (data.length > 0) {
					$.each(data, function (i, obj) {
						$.each(jsond, function (i, v) {
							if(v.id != obj.id) {
								t = true;
								return true;
							}else{
								t = false;
								return false;
							}
						});

						if (t) {
							div += '<p class="row-problem-modal" id="'+obj.id+'"><label lcass>'+obj.name+'</label> <input class="quantity-problem" type="text" placeholder="Quantity"></input> <button class="btn-add-row-problem">Add</button><p>';
						}
						
					});
					
					$("#result-search-problem").html(div);
				}else{
					$("#result-search-problem").html('...');
				}


			});
		}else{
			$("#result-search-problem").html('...');
		}
	});

	$('.modal').on('click', '.btn-add-row-problem', function () {
		
		var row = $(this).parent();
		var d = '';
		var id = row.attr('id');
		var description_problem = $(row).children('label').text();
		var quantity =  $(row).children('input').val();
		if (quantity > 0) {
			if ($.isNumeric(quantity)) {
			d = '<span class="add-problem-effciency">'+description_problem+' ('+quantity+') </span>';
				createJSONProblems(id,quantity);
			}else{
				//$(row).children('div').text('Error!');
			}
			$(row).remove();
			$("#result-problem-efficiency").append(d);
		}else{
			alert("Quantity!");
		}
		


	});



});