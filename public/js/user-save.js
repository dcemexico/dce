$(function () {
	$("#delete-user-photo").on("click", function () {
		var user_id = $("#user_id").val();
		$.ajax({
			url: "/users/removephoto/"+user_id,
			timeout: 10000,
			beforeSend: function () {
			},error: function () {
				alert('Error!')
			}, success: function (res) {
				if (res == 0) {
					$("#user-photo").attr('src', '/images/users/nophoto.png');
					$("#delete-user-photo").remove();
				}else{
					alert('Error!');
				}
			}
		});
	});
});