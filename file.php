<?php
   class AuthController extends BaseController{
      public function getLogin(){
         return View::make('auth.login');
      }
      public function postLogin(){
         $user_data = array(
               'nick' => Input::get('nick'),
                  'password' => Input::get('password'),
                  'active' => true
            );
         
         if (Auth::attempt($user_data)) {
            return Redirect::to('auth/panel');
         }else{
            return $this->getLogin()->with('error', 'Usuario o contraseña incorrecta!');
         }
      }
      public function getPanel(){
         if (Auth::check()) {
            return View::make('panel.index')->with('user', $user);
         }else{
            return $this->getLogin();
         }
      }
      public function getLogout(){
         if(Auth::check()){
            Auth::logout();
         }
         return Redirect::to('auth/login');
      }

   }
?>